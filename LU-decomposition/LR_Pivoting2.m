%%% Numerik Praktikum
%%% Christopher Deitmers 

%%% Aufgabe 4
%%% LR-Zerlegung mit totaler Pivotisierung
%%% LU-decomposition with complete pivoting

%%% Eingabe: Matrix A mit A: R^n -> R^n
%%% Ausgabe: linksuntere Dreiecksmatrix L
%%%          rechtsobere Dreiecksmatrix R
%%%          Zeilenpermutationsmatrix P
%%%          Spaltenpermutationsmatrix Q
%%%          mit P*A*Q = L*R

%%% Funktionsweise:
    %%% siehe LR_Pivoting1
    %%%     Unterschied in 1.1 finde Pivotelement und Permutation
    %%%         beziehe betragsmäßig größtes Element aus allen übrigen
    %%%         Elementen (links untere Restmatrix)
    %%%         Spaltenpermutation via Q(j)
    %%%     Q=Q(1)*...Q(n-1)
    %%% sonst analog
    
    
function [P,L,R,Q] = LR_Pivoting2(A)


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    [~,n] = size(A);

    R = A;
    K = zeros(n,n,n-1); %(n-1)-mal eine nxn-Matrix, die 0 Gaußelimination je Spalte
    K_tild = zeros(n,n,n-2);
    P = zeros(n,n,n-1);
    Q = zeros(n,n,n-1);
    
    for j=1:(n-1)
        %%1.1 finde pivotelement
        [pivots,pivots_lines] = max(abs(R(j:n,j:n)));
        %%% pivots ist vektor der betragsmäßigen max, je spalte
        [pivot,pivot_column]  = max(pivots);
        
        pivot_line   = pivots_lines(pivot_column)+j-1;
        pivot_column = pivot_column+j-1;
        
        Q(:,:,j)=eye(n);
        Q(:,[j,pivot_column],j)=Q(:,[pivot_column,j],j);
        
        P(:,:,j)=eye(n);     
        P([j,pivot_line],:,j)=P([pivot_line,j],:,j);
        
        R=P(:,:,j)*R*Q(:,:,j);
        
        %%1.2
        K(:,:,j) = eye(n);
        for k=j+1:n  
            K(k,j,j) = -( R(k,j)/R(j,j)); 
        end 
        R=K(:,:,j)*R;
    end
    
    %%% 2. K_tild und P und Q
    P_help = P(:,:,1);
    Q_help = Q(:,:,1);
    for j=1:n-2
        P_help = P(:,:,j+1)*P_help;
        Q_help = Q_help*Q(:,:,j+1);
        K_tild(:,:,j) = K(:,:,j);
        for i=j+1:n-1
            K_tild(:,:,j) = P(:,:,i)*K_tild(:,:,j)*P(:,:,i);
        end
    end
    P= P_help;
    Q= Q_help;
    
    %%% 3. L
    L = inv(K(:,:,n-1));
    for i=2:n-1
        L= inv(K_tild(:,:,n-i))*L;
    end

    
end
    
    
    
    
    
