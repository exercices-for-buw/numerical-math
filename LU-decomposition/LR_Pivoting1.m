%%% Numerik Praktikum
%%% Christopher Deitmers

%%% Aufgabe 4
%%% LR-Zerlegung mit partieller Pivotisierung 
%%% LU-decomposition with partial pivoting

%%% Eingabe: Matrix A mit A: R^n -> R^n
%%% Ausgabe: linksuntere Dreiecksmatrix L
%%%          rechtsobere Dreiecksmatrix R
%%%          Zeilenpermutationsmatrix P
%%%          mit P*A = L*R

%%% Funktionsweise: 
%%%
%%%     finde betragsmäßig größten Eintrag je Spalte j unterhalb der
%%%     Diagonalen und permutiere ihn auf die Diagonale R(j,j)
%%%     erzeuge 0 unterhalb der Diagonalen durch elementare Zeilenumformung
%%%     (Gauß), mithilfe von Elementarmatrizen N
%%%     speichere N*..*N je Spalte j in K(j), speichere P(j), 
%%%         konstruiere daraus L, während R iterativ erzeugt wird
%%%         Ansatz: K(n-1)*P(n-1)*...*K(2)*P(2)*K(1)*P(1)*A
%%%                 = K(n-1)*K_tild(n-2)*...*K_tild(1)*P(n-1)*...*P(1)*A
%%%                 für K_tild(k)= P(n-1)*..*P(k+1)*K(k)*P(k+1)*...*P(n-1)
%%%                 dann ist L= K(n-1)*K_tild(n-2)*..*K_tild(1)
%%%                      und P= P(1)*...*P(n-1)
%%% 
%%%     dazu: 1. gehe spalte zu spalte (Zähler j)
%%%             1.1  finde und permutiere Pivotelement durch Matrix P(j)
%%%                  R=P*R
%%%             1.2  gehe ab zeile j+1 nach unten (zähler k)
%%%                     erzeuge K=N(j+1)*...*N(n),
%%%                     erzeuge 0: R=K*R
%%%           2. erzeuge K_tild und P
%%%           3. erzeuge L


function [P,L,R] = LR_Pivoting1(A)

    [~,n] = size(A);

    R = A;
    K = zeros(n,n,n-1); %(n-1)-mal eine nxn-Matrix, die 0 Gaußelimination je Spalte
    K_tild = zeros(n,n,n-2);
    P = zeros(n,n,n-1);
    
    for j=1:(n-1)
        %%1.1
        [pivot,pivot_line] = max(abs(R(j:n,j)));
        pivot_line=pivot_line+j-1;

        P(:,:,j)=eye(n);     %Permuationsmatrix in Schritt j
        P([j,pivot_line],:,j)=P([pivot_line,j],:,j);

        R=P(:,:,j)*R;
        %%1.2
        K(:,:,j) = eye(n);
        for k=j+1:n          
            K(k,j,j) = -( R(k,j)/R(j,j)); 
        end 
        R=K(:,:,j)*R;  %%% eliminiere Einträge R(j+1:n,j) 
    end
    
    %%% 2. K_tild und P
    P_help = P(:,:,1);
    for j=1:n-2
        P_help = P(:,:,j+1)*P_help
        K_tild(:,:,j) = K(:,:,j);
        for i=j+1:n-1
            K_tild(:,:,j) = P(:,:,i)*K_tild(:,:,j)*P(:,:,i);
        end
    end
    P=P_help;
    
    
    %% 3. L
    L = inv(K(:,:,n-1));
    for i=2:n-1
        L= inv(K_tild(:,:,n-i))*L;
    end

end
    