%%% Numerik Praktikum
%%% Christopher Deitmers

%%% Aufgabe 4
%%% LR-Zerlegung ohne Pivotisierung
%%% LU-decomposition without pivoting

%%% Eingabe: Matrix A mit A: R^n -> R^n
%%% Ausgabe: linksuntere Dreiecksmatrix L
%%%          rechtsobere Dreiecksmatrix R
%%%          mit A = L*R

%%% Funktionsweise: 
%%%     erzeuge 0 unterhalb der Diagonalen durch elementare Zeilenumformung
%%%     (Gauß), mithilfe von Elementarmatrizen N, deren (inverses) Produkt durch L
%%%     gegeben ist: N*...*N*A=R, L=inv(N*...*N)
%%%     dazu: gehe spalte zu spalte (Zähler j)
%%%             in Spalte j gehe ab zeile j+1 nach unten (zähler k)
%%%                 erzeuge N, erzeuge 0 in R(k,j) via N*R
%%%                 ergänze L

function [L,R] = LR_Pivoting0(A)

    [~,n] = size(A);
    L = eye(n); 
    R = A;
    %erzeuge 0 unter der diagonalen
    for j = 1:n-1           %Spalte j links nach rechts
        for k = j+1:n       %unterhalb R(j,j) zeile für zeile
            %%% bestimme Elementarmatrix N(-(R_kj/R_jj))
            N = eye(n);
            N(k,j) = -( R(k,j)/R(j,j));
            %%% ergänze L
            L(k,j) = R(k,j)/R(j,j);
            %%% eliminiere Eintrag R(j,k)
            R=N*R;
        end
    end
end

    
