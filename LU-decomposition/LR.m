%%% Numerik Praktikum
%%% Christopher Deitmers

%%% Aufgabe 4
%%% LR-Zerlegung mit möglicher Pivotisierung
%%% LU-decomposition with pivoting on demand (without, partial, complete)

%%% Eingabe: Matrix A mit A: R^n -> R^n
%%%          optionale Eingabe (erlaubt: 0,1,2)
%%% Ausgabe: linksuntere Dreiecksmatrix L
%%%          rechtsobere Dreiecksmatrix R
%%%          optional P,Q
%%%
%%% Funktionsweise:
%%%     falls keine optionale Eingabe: ohne pivotisierung
%%%     erlaubte optionale Eingabe 0/1/2
%%%     0: ohne pivotisierung
%%%     1: partielle pivotisierung
%%%     2: totale pivotisierung


function [L,R,varargout] = LR(A,varargin) 
%%% varargin bietet optionale Eingabeparameter, hier fürs Pivoting

    

    %%% falls keine oder falsche Angabe zum Pivoting gemacht wird
    if nargin < 2 %% number of LR() inputs
        pivoting = 0;
    elseif nargin ~= 2 || varargin{1}<0 || 2<varargin{1} || round(varargin{1})~=varargin{1}
        disp("falsche Eingabe")
        return;
    else 
        pivoting = varargin{1}
    end    
    
    switch pivoting
        %%% ohne pivoting 
        case 0
            [L,R] = LR_Pivoting0(A);
        %%% mit partieller pivotisierung    
        case 1 
            [P,L,R] = LR_Pivoting1(A);
            varargout{1}=P;
        %%% mit totaler pivotisierung
        case 2
            [P,L,R,Q] = LR_Pivoting2(A);
            varargout{1}=P;
            varargout{2}=Q;

    end

    
end
    
    
    
    
    
    
