%%% Numerik Praktikum
%%% Christopher Deitmers

%%% Aufgabe 9
%%% find x_Hat, so that ||A*x_Hat-b||_2 = min||A*x-b||_2 via
%%% 1. (A'*A)*x= A'*b   
%%%    Cholesky-decomposition, method = 0, default
%%% 2. min|Ax-b| = min|Q*Ax-Qb| = min|Q*(Q'*R)*x-Q*b| = min|R*x-Q*b|
%%%    x_Hat = inv(R)*(Q*b)
%%%    Givens-Rotation, method = 1
%%% 3. Ax=b <=> x= inv(A)*A=b // A nicht invertierbar -> A_plus
%%%    pseudoinvers
%%%    Moore-Penrose Pseudo-Inverse, method = 2

function x = LinReg(A,b,varargin)
    
    if nargin < 3
        method = 0
    elseif varargin{1} == round(varargin{1}) && nargin<4 && 0 <= varargin{1} && varargin{1} < 3
        method = varargin{1};
    else
        disp('falsche eingabe');
    end
    
    switch method
        case 0
            [x,~] = solver(A'*A,A'*b);
        case 1
            [Q,R] = GivensRotation(A);
            %%% R evtl nicht quadratisch, also Rückwärtssubstitution statt
            %%% x=inv(R)*(Q*b)
            [~,n] = size(R);
            b=Q*b;
            x(n)= b(n)/R(n,n);
            for i=1:n-1
                x(n-i)= (b(n-i)-R(n-i,n-i+1:n)*x(n-i+1:n)')/R(n-i,n-i);
            end
            x=x';

        case 2
            A_plus = MPPi(A);
            x = A_plus*b;
    end
    
end
