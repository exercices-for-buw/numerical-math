%%% Numerik Praktikum
%%% Christopher Deitmers
%%% updated: 12.4.2021

%%% Aufgabe 6
%%% Konditionszahl bzgl spektral norm (1)/Zeilensummen Norm (2)
%%% condition number re spectral norm (1)/ row-sum norm (2)

%%% input: Matrix A
%%%        optionale Eingabe 
%%%             2 liefert cond bzgl spec norm (auch ohne optionale eingabe)
%%%             1 liefert cond bzgl row sum norm


function cond = cond(A,varargin)
    
    if nargin < 2
        norm = 2;
    elseif nargin > 2
        disp('falsche eingabe');
        return;
    elseif varargin{1} == 1 || varargin{1} == 2
        norm = varargin{1};
    else 
        disp('falsche eingabe');
        return;
    end
    
    switch norm
        case 2
            cond = spec_norm(A)*spec_norm(inv(A));
        case 1
            cond = row_sum_norm(A)*row_sum_norm(inv(A));
    end

end
