%%% Numerik Praktikum
%%% Christopher Deitmers
%%% updated: 12.4.2021

%%% Aufgabe 6
%%% spectral norm: lub-norm re lp_2

function sp = spec_norm(A)

    lambda = eig(A'*A);
    [s,~] = max(lambda);
    sp=sqrt(s);
    
end
