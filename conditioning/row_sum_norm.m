%%% Numerik Praktikum
%%% Christopher Deitmers
%%% updated: 12.4.2021

%%% Aufgabe 6
%%% row-sum norm: lub-norm re lp_inf


function N = row_sum_norm(A)

   [m,n] = size(A); 
   N=0;
    for j = 1:m
        if sum(abs(A(j,:)))>N
            N = sum(abs(A(j,:)));
        end
    end
end

        
  

    