%%% Numerik Praktikum
%%% Christopher Deitmers
%%% updated: 12.4.2021

%%% Aufgabe 6
%%% solving linear equation Ax=b via LU-/Cholesky-decomposition
%%% inclusive error estimation

%%% input: Matrix A
%%%        vektor b
%%%        optionale Eingabe 
%%%             2 liefert fehler bzgl spec norm (auch ohne optionale eingabe)
%%%             1 liefert cond bzgl row sum norm
%%% output: Lösungsvektor x 


function [x,est] = solver(A,b,varargin)
    
    [~,n]=size(A);
    y=zeros(1,n);
    x=zeros(1,n);
%%%%%%%%%%%%%%%%%%%% Norm %%%%%%%%%%
    if nargin < 3
        norm = 2;
    elseif nargin > 2
        disp('falsche eingabe');
        return;
    elseif varargin{1} == 1 || varargin{1} == 2
        norm = varargin{1};
    else 
        disp('falsche eingabe');
        return;
    end
    %%%%%%%%%%%%%%%%%%%%%%%% 
    
    L_hat = Cholesky(A);
    if L_hat == 1
        L_hat = Cholesky(-A);
    end
    if L_hat == 0 
        %%%%%%%%% LR Zerlegung
        [L,R]=LR_Pivoting0(A);
    else 
        %%% Cholesky worked 
        L=L_hat;
        R=L_hat';
    end
        
    %%% Vorwärtsubsitution
    y(1)= b(1)/L(1,1);
    for i=2:n
        y(i)= (b(i)-L(i,1:i-1)*y(1:i-1)')/L(i,i);
    end
    %%%%%%%%% Rückwärtssubstitution R*x=y
    x(n)= y(n)/R(n,n);
    for i=1:n-1
        x(n-i)= (y(n-i)-R(n-i,n-i+1:n)*x(n-i+1:n)')/R(n-i,n-i);
    end
    x=x';
    
    switch norm
        case 1
            est= spec_norm(inv(A))*max(abs(b-A*x));
            s='Maximumsnorm';
        case 2    
            est= row_sum_norm(inv(A))*sqrt(sum( (b-A*x).^2 ));
            s='Euklidischen Norm';
    end
    
    fprintf('Unter Verwendung der %s bleibt der Fehler zum analytischen Ergebnis kleinergleich %.8f',s,est);
    
end
