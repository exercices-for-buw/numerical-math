%%% Numerik Praktikum
%%% Christopher Deitmers 
%%% updated: 12.4.2021

%%% Aufgabe 5
%%% Cholesky-Zerlegung

%%% Vorteil ggü LR: halber Speicher, halber Aufwand

%%% Eingabe: Matrix A mit A: R^n -> R^n
%%% Ausgabe: 

%%% Funktionsweise:
%%%
%%%     funktioniert bei symmetrischen positiv-definiten Matrizen, 
%%%     dann gelten A=A' und R(j,j)>0 
%%%     Ansatz: A=L*R= L*D*inv(D)*R mit D=diag(R), R_tild=inv(D)*R
%%%             L*D*R_tild=A=A'=R_tild'*D*L'
%%%             L,R eindeutig: L=R_tild, also A=L*D*L
%%%     für D^(1/2)= diag(sqrt(R(j,j))), L_hat= L*D^(1/2) 
%%%         ist dann A= L_hat*L_hat'
%%%     aus der Symmetrie von A folgt der Algorithmus
%%%
%%%     falls die Bedingungen an A nicht erfüllt sind: Fehlermeldung

function L_hat = Cholesky(A)
    
    [~,n]=size(A);
    %%% L_hat untere Dreiecksmatrix hat nur einen Eintrag in Zeile 1
    %%% auf L_hat(1,1-1) kann nicht zugegriffen werden
    %%% Extrafall i=1 der folgenden Iteration
    sqr_help= A(1,1);
    if sqr_help > 0
        L_hat(1,1)=sqrt(sqr_help);
    else
        disp('A ist nicht symmetrisch und positiv-definit');
        L_hat = 1;
        return
    end
    for j=2:n
        L_hat(j,1)=A(j,1)/L_hat(1,1);
    end
    
    %%% folgende Iteration
    for i=2:n
        %%% bestimme Diagonalelement
        sqr_help=A(i,i)-sum( L_hat(i,1:i-1).^2 );
        if sqr_help > 0
            L_hat(i,i)=sqrt(sqr_help);
        else 
            disp('A ist nicht symmertrisch und positiv-definit');
            L_hat = 0;
            return
        end
        %%% bestimme Eintrage unter dem Diagonalelement
        for j=(i+1):n
            L_hat(j,i)=(A(j,i)-(L_hat(j,1:i-1)*L_hat(i,1:i-1)') )/L_hat(i,i);
        end
    end


end
